FROM alpine as build
RUN apk add git gcc g++ cmake make libgcc libstdc++ && git clone https://github.com/sass/libsass.git && cd libsass && make && cd .. && git clone https://github.com/google/re2.git && cd re2 && make &&  chmod 0644 re2/filtered_re2.h re2/re2.h re2/set.h re2/stringpiece.h re2.pc

FROM alpine as prod
COPY --from=build /re2/obj/libre2.a /libsass/lib/libsass.a /usr/local/lib/
COPY --from=build /re2/re2/filtered_re2.h /re2/re2/re2.h /re2/re2/set.h /re2/re2/stringpiece.h /re2/re2.pc /usr/local/lib/pkgconfig/
COPY --from=build /re2/obj/so/libre2.so.9 /usr/local/lib/libre2.so.9.0.0
RUN apk --no-cache add libgcc libstdc++ && mkdir -p /usr/local/include/re2 /usr/local/lib/pkgconfig && sed -i -e "s#@includedir@#/usr/local/include#" /usr/local/lib/pkgconfig/re2.pc && sed -i -e "s#@libdir@#/usr/local/lib#" /usr/local/lib/pkgconfig/re2.pc && ln -sf libre2.so.9.0.0 /usr/local/lib/libre2.so.9 && ln -sf libre2.so.9.0.0 /usr/local/lib/libre2.so





